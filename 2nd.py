import pandas as pd
from jdatetime import date, time, datetime


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]


class MyClass(metaclass=Singleton):
    def __init__(self, DateTimeAdd, DateTimeRemind, info, num):
        self.DTA = DateTimeAdd  # DTA: DateTimeAdd
        self.DTR = DateTimeRemind  # DTR: DateTimeRemind
        self.info = info
        self.num = num

    def create_reminder(self):
        main_df = pd.DataFrame()
        new_row = {"DateTimeAdd": self.DTA, 'DateTimeRemind': self.DTR, 'info': self.info, 'num': self.num, "Done": 0}
        main_df = main_df.append(new_row, ignore_index=True)
        with open("data.csv", 'a') as f:
            main_df.to_csv(f, header=f.tell() == 0, index=False)

    @staticmethod
    def ahead_reminders():
        df = pd.read_csv("data.csv")
        ndate = datetime.now()
        for row in df.iterrows():
            obj1 = datetime.strptime(row[1]["DateTimeRemind"], '%Y-%m-%d %H:%M:%S')
            if obj1 > ndate:
                pass
            else:
                df = df.drop([row[0]], axis=0)

        print(df)

    @staticmethod
    def check_reminder(num):
        df = pd.read_csv("data.csv")
        df._set_value(num, "Done", 1)
        df.to_csv("data.csv", index=False)

    @staticmethod
    def remove_row(n):
        df = pd.read_csv("data.csv")
        df.drop(df[df["num"] == n].index, inplace=True)
        df.to_csv("data.csv", index=False)

    @staticmethod
    def search(num):
        df = pd.read_csv("data.csv")
        if df[df["num"] == num].empty:
            print("No reminder")
        else:
            print(df[df["num"] == num])

    class print:

        @staticmethod
        def all():
            df = pd.read_csv("data.csv")
            print(df)

        @staticmethod
        def done():
            df1 = pd.read_csv("data.csv")
            print(df1[df1["Done"] == 1])

        @staticmethod
        def not_done():
            df2 = pd.read_csv("data.csv")
            print(df2[df2["Done"] == 0])


if __name__ == '__main__':
    obj = MyClass()
    obj.ahead_reminders()