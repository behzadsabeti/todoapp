from tkinter import *


class app:

    def __init__(self, master):
        self.r = master
        master.geometry("400x600")
        self.LogRig = Frame(master)
        self.main_menu = Frame(master)
        self.log = Frame(master)
        self.rig = Frame(master)

    def log_rig(self, frame=None):

        if frame is not None:
            frame.forget()

        self.LogRig.pack()
        myLabel = Label(self.LogRig, text="To Do Application", padx=105, pady=30)
        myButton1 = Button(self.LogRig, text="log in", command=lambda: self.login(self.LogRig), padx=105, pady=30)
        myButton2 = Button(self.LogRig, text="register", command=lambda: self.register(self.LogRig), padx=100, pady=30)
        myLabel.grid(row=0, column=0)
        myButton1.grid(row=1, column=0)
        myButton2.grid(row=2, column=0)

    def login(self, frame):
        frame.forget()
        self.log.pack()
        e1 = Entry(self.log, width=35, borderwidth=5)
        e2 = Entry(self.log, width=35, borderwidth=5)
        label1 = Label(self.log, text="username:")
        label2 = Label(self.log, text="password:")
        button1 = Button(self.log, text="back", command=lambda: self.log_rig(self.log))
        button2 = Button(self.log, text="enter")

        label1.grid(row=0, column=0)
        label2.grid(row=1, column=0)
        e1.grid(row=0, column=1)
        e2.grid(row=1, column=1)
        button1.grid(row=2, column=0)
        button2.grid(row=2, column=1)

    def register(self, frame):
        frame.forget()
        self.rig.pack()
        label1 = Label(self.rig, text="enter a username:")
        label2 = Label(self.rig, text="enter your password:")
        label3 = Label(self.rig, text="enter your password again:")
        e1 = Entry(self.rig, width=35, borderwidth=5)
        e2 = Entry(self.rig, width=35, borderwidth=5)
        e3 = Entry(self.rig, width=35, borderwidth=5)
        button1 = Button(self.rig, text="back", command=lambda: self.log_rig(self.rig))
        button2 = Button(self.rig, text="enter")

        label1.grid(row=0, column=0)
        label2.grid(row=1, column=0)
        label3.grid(row=2, column=0)
        e1.grid(row=0, column=1)
        e2.grid(row=1, column=1)
        e3.grid(row=2, column=1)
        button1.grid(row=3, column=0)
        button2.grid(row=3, column=1)


if __name__ == "__main__":
    root = Tk()
    a = app(root)
    a.log_rig()
    root.mainloop()
