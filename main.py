import pandas as pd
from jdatetime import date, time, datetime
from tkinter import *
import re
global username


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]


class MyClass(metaclass=Singleton):
    def __init__(self, DateTimeAdd, DateTimeRemind, info, num):
        self.DTA = DateTimeAdd  # DTA: DateTimeAdd
        self.DTR = DateTimeRemind  # DTR: DateTimeRemind
        self.info = info
        self.num = num

    def create_reminder(self):
        main_df = pd.DataFrame()
        new_row = {"DateTimeAdd": self.DTA, 'DateTimeRemind': self.DTR, 'info': self.info, 'num': self.num, "State": "Not Done"}
        main_df = main_df.append(new_row, ignore_index=True)
        with open("data{}.csv".format(username), 'a') as f:
            main_df.to_csv(f, header=f.tell() == 0, index=False)

    @staticmethod
    def ahead_reminders():
        df = pd.read_csv("data{}.csv".format(username))
        ndate = datetime.now()
        for row in df.iterrows():
            obj1 = datetime.strptime(row[1]["DateTimeRemind"], '%Y-%m-%d %H:%M:%S')
            if obj1 > ndate:
                pass
            else:
                df = df.drop([row[0]], axis=0)

        return df

    @staticmethod
    def check_reminder(num):
        df = pd.read_csv("data{}.csv".format(username), index_col="num")
        df.loc[num, "State"] = "Done"
        df.to_csv("data{}.csv".format(username))

    @staticmethod
    def remove_row(n):
        df = pd.read_csv("data{}.csv".format(username))
        df.drop(df[df["num"] == n].index, inplace=True)
        df.to_csv("data{}.csv".format(username), index=False)

    @staticmethod
    def exists(num):
        df = pd.read_csv("data{}.csv".format(username))
        if df[df["num"] == num].empty:
            return False
        else:
            return True

    @staticmethod
    def search(num):

        df = pd.read_csv("data{}.csv".format(username))
        return df[df["num"] == num]

    @staticmethod
    def search_by_info(str_):
        df = pd.read_csv("data{}.csv".format(username))
        x = df.filter(items=["info", "num"])
        x = x.set_index("info")
        dict_ = x.to_dict()["num"]
        main_df = pd.DataFrame(columns=["DateTimeAdd", "DateTimeRemind", "State", "info", "num"])
        for this in dict_.keys():

            if re.search(str_, this):
                new_df = MyClass.search(dict_[this])
                main_df = main_df.append(new_df, ignore_index=True)

        return main_df

    class print:

        @staticmethod
        def all():
            df = pd.read_csv("data{}.csv".format(username))
            return df

        @staticmethod
        def done():
            df1 = pd.read_csv("data{}.csv".format(username))
            return df1[df1["State"] == "Done"]

        @staticmethod
        def not_done():
            df2 = pd.read_csv("data{}.csv".format(username))
            return df2[df2["State"] == "Not Done"]


class login(metaclass=Singleton):

    def __init__(self, user, pass_):
        self.username = user
        self.password = pass_

        global username
        username = user

    def new_user(self):
        new_df = pd.DataFrame()
        new_row = {"username": self.username, 'password': str(self.password)}
        new_df = new_df.append(new_row, ignore_index=True)
        with open("user_data.csv", "a") as f:
            new_df.to_csv(f, header=f.tell() == 0, index=False)

    def check_data(self):
        df = pd.read_csv("user_data.csv")
        df = df.set_index("username")
        dict_ = df.to_dict()["password"]
        if self.username in dict_.keys():
            if self.password == dict_[self.username]:
                return "already_exists"
            else:
                return "wrong_password"
        else:
            return "new_user"

    def main(self):
        if self.check_data() == "already_exists":
            return True
        else:
            return False


class reg(login, metaclass=Singleton):

    def create(self):

        if self.check_data() == "new_user":
            return True
        else:
            return False


class app:

    def __init__(self, master):
        self.r = master
        master.geometry("600x889")
        self.LogRig = Frame(master)
        self.main_menu = Frame(master)
        self.log = Frame(master)
        self.rig = Frame(master)
        self.CR = Frame(master)        # CR: create reminder
        self.AR = Frame(master)        # AR: Ahead reminders
        self.DN = Frame(master)
        self.search = Frame(master)
        self.SCH = Frame(master)

    def log_rig(self, frame=None):

        if frame is not None:
            frame.forget()

        self.LogRig.pack()
        myLabel = Label(self.LogRig, text="To Do Application", padx=105, pady=30)
        myButton1 = Button(self.LogRig, text="log in", command=lambda: self.login(self.LogRig), padx=105, pady=30)
        myButton2 = Button(self.LogRig, text="register", command=lambda: self.register(self.LogRig), padx=100, pady=30)
        myLabel.grid(row=0, column=0)
        myButton1.grid(row=1, column=0)
        myButton2.grid(row=2, column=0)

    def login(self, frame):
        def click_login(u, p):
            obj = login(str(u), int(p))
            if obj.main():
                self.Main_Menu(self.log)
            else:
                label = Label(self.log, text=obj.check_data())
                label.grid(row=3, column=0)
        frame.forget()
        self.log.pack()
        e1 = Entry(self.log, width=35, borderwidth=5)
        e2 = Entry(self.log, show="*", width=35, borderwidth=5)
        label1 = Label(self.log, text="username:")
        label2 = Label(self.log, text="password:")
        button1 = Button(self.log, text="back", command=lambda: self.log_rig(self.log))
        button2 = Button(self.log, text="enter", command=lambda: click_login(e1.get(), e2.get()))

        label1.grid(row=0, column=0)
        label2.grid(row=1, column=0)
        e1.grid(row=0, column=1)
        e2.grid(row=1, column=1)
        button1.grid(row=2, column=0)
        button2.grid(row=2, column=1)

    def register(self, frame):

        def click_reg(u, p, p_):
            if p != p_:
                label = Label(self.rig, text="password are not the same")
                label.grid(row=4, column=0)
            else:
                obj = reg(str(u), int(p))
                if obj.create():
                    obj.new_user()
                    label = Label(self.rig, text="user created, go back and login")
                    label.grid(row=4, column=0)
                else:
                    label = Label(self.rig, text="this username is already taken")
                    label.grid(row=4, column=0)

        frame.forget()
        self.rig.pack()
        label1 = Label(self.rig, text="enter a username:")
        label2 = Label(self.rig, text="enter your password:")
        label3 = Label(self.rig, text="enter your password again:")
        e1 = Entry(self.rig, width=35, borderwidth=5)
        e2 = Entry(self.rig, width=35, borderwidth=5)
        e3 = Entry(self.rig, width=35, borderwidth=5)
        button1 = Button(self.rig, text="back", command=lambda: self.log_rig(self.rig))
        button2 = Button(self.rig, text="enter", command=lambda: click_reg(e1.get(), e2.get(), e3.get()))

        label1.grid(row=0, column=0)
        label2.grid(row=1, column=0)
        label3.grid(row=2, column=0)
        e1.grid(row=0, column=1)
        e2.grid(row=1, column=1)
        e3.grid(row=2, column=1)
        button1.grid(row=3, column=0)
        button2.grid(row=3, column=1)

    def Main_Menu(self, frame):

        frame.forget()
        self.main_menu.pack()
        myLabel = Label(self.main_menu, text="Main Menu", padx=105, pady=30)
        button1 = Button(self.main_menu, text="Create Reminder", padx=105, pady=30, command=lambda: self.create_reminder(self.main_menu))
        button2 = Button(self.main_menu, text="Ahead Reminders", padx=102, pady=30, command=self.click_AheadReminder)
        button3 = Button(self.main_menu, text="Done Reminders", padx=105, pady=30, command=lambda: self.print_reminders(self.main_menu, MyClass.print.done()))
        button4 = Button(self.main_menu, text="Not Done Reminders", padx=95, pady=30, command=lambda: self.print_reminders(self.main_menu, MyClass.print.not_done()))
        button5 = Button(self.main_menu, text="Search Reminder", padx=105, pady=30, command=lambda: self.search_reminder(self.main_menu))
        button6 = Button(self.main_menu, text="History Of Reminders", padx=94, pady=30, command=lambda: self.print_reminders(self.main_menu, MyClass.print.all()))

        myLabel.grid(row=0, column=0)
        button1.grid(row=1, column=0)
        button2.grid(row=2, column=0)
        button3.grid(row=3, column=0)
        button4.grid(row=4, column=0)
        button5.grid(row=5, column=0)
        button6.grid(row=6, column=0)

    def create_reminder(self, frame):

        def click_create(d, t, i, n):
            e1.delete(0, 'end')
            e2.delete(0, 'end')
            e3.delete(0, 'end')
            e4.delete(0, 'end')
            date_time = d + " " + t
            obj = MyClass(datetime.now().replace(microsecond=0), date_time, i, n)
            obj.create_reminder()
            myLabel5 = Label(self.CR, text="your reminder was been created")
            myLabel5.grid(row=5, column=0)

        frame.forget()
        self.CR.pack()
        myLabel1 = Label(self.CR, text="enter the date(ex: 1399-10-30):", font=19)
        myLabel2 = Label(self.CR, text="enter the time(ex: 22:31:15):", font=19)
        myLabel3 = Label(self.CR, text="enter the info(ex: emtehan):", font=19)
        myLabel4 = Label(self.CR, text="enter the number(ex: 124):", font=19)
        e1 = Entry(self.CR, width=25, borderwidth=5)
        e2 = Entry(self.CR, width=25, borderwidth=5)
        e3 = Entry(self.CR, width=25, borderwidth=5)
        e4 = Entry(self.CR, width=25, borderwidth=5)
        button1 = Button(self.CR, text="create", padx=65, pady=8, command=lambda: click_create(e1.get(), e2.get(), e3.get(), e4.get()))
        button2 = Button(self.CR, text="back", padx=65, pady=8, command=lambda: self.Main_Menu(self.CR))

        myLabel1.grid(row=0, column=0)
        myLabel2.grid(row=1, column=0)
        myLabel3.grid(row=2, column=0)
        myLabel4.grid(row=3, column=0)
        e1.grid(row=0, column=1)
        e2.grid(row=1, column=1)
        e3.grid(row=2, column=1)
        e4.grid(row=3, column=1)
        button1.grid(row=4, column=1)
        button2.grid(row=4, column=0)

    def click_back_ahead(self):
        self.Main_Menu(self.AR)
        self.DN.forget()
        self.SCH.forget()

    def print_reminders(self, frame, df):

        frame.forget()
        self.AR.pack()
        list_of_entries1 = df["DateTimeAdd"].values.tolist()
        list_of_entries2 = df["DateTimeRemind"].values.tolist()
        list_of_entries3 = df["info"].values.tolist()
        list_of_entries4 = df["num"].values.tolist()
        list_of_entries5 = df["State"].values.tolist()

        label1 = Label(self.AR, text="DateTimeAdd", font=26)
        label2 = Label(self.AR, text="DateTimeRemind", font=26)
        label3 = Label(self.AR, text="information", font=26)
        label4 = Label(self.AR, text="number", font=26)
        label5 = Label(self.AR, text="State", font=26)
        button = Button(self.AR, text="back", font=26, command=self.click_back_ahead)

        var1 = StringVar(value=list_of_entries1)
        var2 = StringVar(value=list_of_entries2)
        var3 = StringVar(value=list_of_entries3)
        var4 = StringVar(value=list_of_entries4)
        var5 = StringVar(value=list_of_entries5)

        listbox1 = Listbox(self.AR, listvariable=var1, font=26, height=30, width=17)
        listbox2 = Listbox(self.AR, listvariable=var2, font=26, height=30, width=17)
        listbox3 = Listbox(self.AR, listvariable=var3, font=26, height=30, width=11)
        listbox4 = Listbox(self.AR, listvariable=var4, font=26, height=30, width=8)
        listbox5 = Listbox(self.AR, listvariable=var5, font=26, height=30, width=8)

        label1.grid(row=0, column=0)
        label2.grid(row=0, column=1)
        label3.grid(row=0, column=2)
        label4.grid(row=0, column=3)
        label5.grid(row=0, column=4)

        listbox1.grid(row=1, column=0)
        listbox2.grid(row=1, column=1)
        listbox3.grid(row=1, column=2)
        listbox4.grid(row=1, column=3)
        listbox5.grid(row=1, column=4)
        button.grid(row=2, column=0)

    def done(self):
        x = self
        self.DN.pack()

        def click_enter():
            MyClass.check_reminder(int(e.get()))
            x.click_AheadReminder()

        label = Label(self.DN, text="which reminder would\nyou done?\nenter the number")
        e = Entry(self.DN, width=10, borderwidth=5)
        button = Button(self.DN, text="Enter", command=click_enter)

        label.grid(row=2, column=1)
        e.grid(row=2, column=2)
        button.grid(row=2, column=3)

    def click_AheadReminder(self):
        self.print_reminders(self.main_menu, MyClass.ahead_reminders())
        self.done()

    def search_reminder(self, frame):
        x = self

        def click_enter(num):
            if not MyClass.exists(num):
                label2 = Label(x.search, text="No reminder")
                label2.grid(row=1, column=0)
            else:
                x.print_reminders(x.search, MyClass.search(num))
            x.SCH.pack()
            label1 = Label(x.SCH, text="Do you wanna remove it?")
            button2 = Button(x.SCH, text="remove", command=lambda: MyClass.remove_row(num))

            label1.grid(row=1, column=1)
            button2.grid(row=1, column=2)

        def click_enter_(num):
            if MyClass.search_by_info(num).empty:
                label2 = Label(x.search, text="No reminder")
                label2.grid(row=2, column=0)
            else:
                x.print_reminders(x.search, MyClass.search_by_info(num))
                x.SCH.pack()
                label1 = Label(x.SCH, text="Do you wanna remove it?")
                button2 = Button(x.SCH, text="remove", command=lambda: MyClass.remove_row(num))

                label1.grid(row=1, column=1)
                button2.grid(row=1, column=2)

        frame.forget()
        self.search.pack()

        label = Label(self.search, text="enter the number:", font=26)
        e = Entry(self.search, width=10, borderwidth=5)
        button = Button(self.search, text="enter", command=lambda: click_enter(int(e.get())))
        button1 = Button(self.search, text="back", command=lambda: self.Main_Menu(self.search))

        label3 = Label(self.search, text="or enter the info:", font=26)
        e3 = Entry(self.search, width=10, borderwidth=5)
        button3 = Button(self.search, text="enter", command=lambda: click_enter_(str(e3.get())))

        label.grid(row=0, column=0)
        e.grid(row=0, column=1)
        button.grid(row=0, column=2)
        button1.grid(row=0, column=3)
        label3.grid(row=1, column=0)
        e3.grid(row=1, column=1)
        button3.grid(row=1, column=2)


if __name__ == '__main__':
    root = Tk()
    a = app(root)
    a.log_rig()
    root.mainloop()
